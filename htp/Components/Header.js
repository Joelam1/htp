import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Icon, Item, Input,
    Header, Right, Left, 
    Title, Button, Body
  } from 'native-base';

import { Image, Text, TouchableHighlight,
    View } from 'react-native'

//Home Screen Header
class HomeHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isNavigating: false,
        };
    }
    handleSearch(text) {
        console.log('searching =>', text)
    }
    render() {
        const {
            handleSearchChanged = (text) => { this.handleSearch(text)},
            search = '',
        } = this.props;
        return (
        <View style={{ backgroundColor: 'black', height: '15%', flexDirection: 'row' }}>
            <View style={{ marginLeft: '3%', marginRight: '4%',justifyContent: 'center'}}>
                <Icon onPress={() => Actions.drawerOpen() }
                    name ="menu"
                    style={{ color: 'white', alignItems: 'center'}} 
                />
            </View>
            <View style={{ flex: 1, height: '90%', justifyContent: 'center', marginTop: '2%' }}>
                <Item style={{ backgroundColor: 'white', paddingHorizontal: 5, borderRadius: 4 }}>
                    <Icon name="search" style={{ fontSize: 20, paddingTop: 5 }} />
                    <Input placeholder="Search" onChangeText={handleSearchChanged} value={search}/>
                </Item>
            </View>
            <TouchableHighlight onPress={()=> Actions.Home()}>
                <Image style ={{
                    flex: 1,
                    width: 60,
                    height:60,
                    marginTop:'12.5%',
                    justifyContent: 'center',
                    resizeMode: 'contain' }}
                    source={require('../img/Logo.png')}
                    resizeMode="contain"
                />
            </TouchableHighlight>
        </View>
    )
    }
}

//First Screen Header Usually the first screen of the product or Warranty, Professional, and etc. screens. 
class AppHeader extends Component {
    render() {
        const { name } = this.props;
        return (
            <Header style={{backgroundColor:'black'}}>
                <Left style={{flex:1}}>
                    <Button transparent 
                        onPress={()=> Actions.drawerOpen()}>
                        <Icon name='menu' style={{color:'white'}}/>
                    </Button>
                </Left>
                <Text style={{flex:1, justifyContent:'center', alignItems:'center', marginTop:'2%'}}>
                    <Title style={{color:'white'}}>
                    { name }
                    </Title>
                </Text>
                <Right style={{flex:1}}>
                    <Button transparent
                        onPress={()=> Actions.Home() }>
                        <Image style={{width:'100%', height:'100%',marginLeft:'37.5%'}}
                            source={require('../img/Logo.png')} 
                            resizeMode='contain'
                            />
                    </Button>
                </Right >
            </Header>
        )
    }
}

//Product Screens mainly in the Water Heaters or Space Heater Sections
class BackAppHeader extends Component {
    render() {
        const { name } = this.props;
        return (
            <Header style={{backgroundColor:'black'}}>
                <Left>
                    <Button transparent 
                        onPress={()=> Actions.pop()}>
                        <Icon name='arrow-back' style={{color:'white', marginLeft: '5%'}}/>
                    </Button>
                </Left>
                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{justifyContent:'center', alignItems:'center',marginTop:'2%'}}>
                    <Title style={{color:'white',fontSize:17}}>
                    { name }
                    </Title>
                </Text>
                <Right>
                    <Button transparent
                        onPress={()=> Actions.Home() }>
                        <Image style={{width:'100%', height:'100%', justifyContent:'center', marginLeft:'37.5%'}}
                            source={require('../img/Logo.png')} 
                            resizeMode='contain'
                            />
                    </Button>
                </Right >
            </Header>
        )
    }
}
//Product Category Back Headers. Residential Or Commercial, Gas Or Electric, and etcs.
class BackHeader extends Component {
    render() {
        const { name } = this.props;
        return (
            <Header style={{backgroundColor:'black'}}>
                <Left>
                    <Button transparent 
                        onPress={()=> Actions.pop()}>
                        <Icon name='arrow-back' style={{color:'white', marginLeft: '5%'}}/>
                    </Button>
                </Left>
                <Text numberOfLines={1} style={{marginTop:'2%', justifyContent:'center', alignItems:'center'}}>
                    <Title style={{color:'white',fontSize:15}}>
                    { name }
                    </Title>
                </Text>
                <Right>
                    <Button transparent
                        onPress={()=> Actions.Home() }>
                        <Image style={{width:'100%', height:'100%',marginLeft:'37.5%'}}
                            source={require('../img/Logo.png')} 
                            resizeMode='contain'
                            />
                    </Button>
                </Right >
            </Header>
        )
    }
}

export { HomeHeader, AppHeader, BackHeader, BackAppHeader }