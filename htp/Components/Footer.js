import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Button, Footer, FooterTab
} from 'native-base';

import { Text } from 'react-native'

//CoR = Commercial or Residential screen
class CoRFoots extends Component {
    render() {
        return (
            <Footer>
                <FooterTab>
                    <Button onPress={() => Actions.push('Home')}>
                        <Text>
                            Home
                        </Text>
                    </Button>
                    <Button onPress={() => Actions.push('Products')}>
                        <Text>
                            Product
                        </Text>
                    </Button>
                    <Button onPress={() => Actions.push('ComOrRes')}>
                        <Text>
                            ComOrRes
                        </Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}

export { CoRFoots }