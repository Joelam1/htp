import React, { Component } from 'react';

import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  TouchableHighlight,
  StatusBar,
  Dimensions,
  Linking,
} from 'react-native';

import {
  Container,
} from 'native-base';

import Swiper from 'react-native-swiper';
import { Actions } from 'react-native-router-flux';
import { HomeHeader } from '../../Components/Header';
import ProductSearchScreen from '../Product/ProductSearch';

var deviceHeight = Dimensions.get("window").height;
class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
    this.handleSearchChanged = this.handleSearchChanged.bind(this);
  }

  handleSearchChanged(search) {
    this.setState({ search })
  }

  isSearching() {
    return (this.state.search && this.state.search.length > 0);
  }

  //This is Home Screen Set Up with images; inbetween Swiper is the slide images ; images belows are all the Icons images in the Home Screens
  render() {
    return (
          <Container style={{ backgroundColor: 'black', alignItems: 'stretch', flexDirection: 'column' }}>
              <HomeHeader search={this.state.search} handleSearchChanged={this.handleSearchChanged}/>
              <View style={{ flex: 1 }}>
                  {this.isSearching() ? <ProductSearchScreen search={this.state.search} /> :
                  <ScrollView>
                      <Swiper
                          autoplay
                          style={{ height: 175, width: null, resizeMode: 'contain'}}
                        >
                          <View style={{ flex: 1 }}>
                              <Image
                                  style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                  source={require('../../img/Home/slide11.jpg')}
                                />
                            </View>
                          <View style={{ flex: 1 }}>
                              <Image
                                  style={{ flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                  source={require('../../img/Home/slide-22.jpg')}
                                />
                            </View>
                          <View style={{ flex: 1 }}>
                              <Image
                                  style={{  flex: 1, height: null, width: null, resizeMode: 'contain' }}
                                  source={require('../../img/Home/slide-33.jpg')}
                                />
                            </View>
                        </Swiper>
                      <View style={styles.MenuContainer}>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight
                                  style={styles.Touch}
                                  onPress={() => Actions.push('Products')}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/Products.png')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                                  onPress={() => Actions.Professional()}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/Pro.png')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                                  onPress={() => Actions.Sales()}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/SalesRep.png')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                                  onPress={() => Actions.Warranty()}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/Warranty.png')}
                                    />
                                </TouchableHighlight>
                          </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                              onPress={()=>Linking.openURL('http://go.htproducts.com/htp-link-product-selection')}>
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/HTP-Link.jpg')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}>
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/TrueFit.jpg')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                                  onPress={() => Actions.Contact()}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/Contact.png')}
                                    />
                                </TouchableHighlight>
                            </View>
                          <View style={styles.MenuItem}>
                              <TouchableHighlight style={styles.Touch}
                                  onPress={() => Actions.Shop()}
                                >
                                  <Image
                                      style={styles.Image}
                                      source={require('../../img/Home/Shop.png')}
                                    />
                                </TouchableHighlight>
                            </View>
                        </View>
                    </ScrollView>}
                </View>
              <StatusBar barStyle="light-content" />
            </Container>
    );
  }
}
export default HomeScreen;

const styles = StyleSheet.create({
  MenuContainer: {
    flex: 1,
    height: '100%',
    width: '100%',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },

  MenuItem: {
    height: deviceHeight/5.5,
    width: '49%',
    backgroundColor: 'black',
    marginLeft: '.5%',
    marginRight: '.5%',
    padding: 5,
  },

  Touch: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: '1%',
    borderRadius: 10,
  },

  Image: {
    height: '100%',
    width: '55%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'contain',
  },

});
