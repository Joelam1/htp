import React from 'react'

import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
    TouchableHighlight,
    StatusBar,
    Dimensions,
    Linking,
} from 'react-native';

import { AppHeader } from '../../Components/Header';
import { Actions } from 'react-native-router-flux';

var deviceHeight = Dimensions.get("window").height;
export default class WarrantyScreen extends React.Component{
    render(){
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <AppHeader name="Warranty" />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Extend')}>
                                <Image
                                    style={styles.Image}
                                    source={require('../../img/Warranty/EProd-War.png')}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('WarReg')}>
                                <Image
                                    style={styles.Image}
                                    source={require('../../img/Warranty/WarExReg.png')}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Wizard')}>
                                <Image
                                    style={styles.Image}
                                    source={require('../../img/Warranty/WarWiz.png')}
                                />
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height:deviceHeight/4,
        width:'100%',
        backgroundColor:'black', 
        padding:5
    },

    Touch:{
        flex:1,
        height: deviceHeight/6, 
        width: '100%' , 
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:'1%',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'contain',
    }

    });