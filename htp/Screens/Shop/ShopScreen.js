import React from 'react';
import { 
	StyleSheet,
	View,
	WebView,
	Image,
	StatusBar,
} from 'react-native';

import { AppHeader } from '../../Components/Header';

export default class ShopScreen extends React.Component {


    render() {        

        return (
			<View style={{flex:1}}>
				<AppHeader name="Shop" />
				<View style={styles.layouts}>
					<View style={styles.Screen}>
						<View style={{ width: '100%', height: '100%', resizeMode:'Center' /* Webivew 'align-items: center' fix */ }}>
							<WebView 
								source={{ uri: "https://shophtp.com/ecommerce/" }}
							/>
						</View>
					</View>
				</View>
				<StatusBar barStyle='light-content'/>
			</View>
        );
    }
}

const styles = StyleSheet.create({

	layouts: {
	    flexDirection: 'row',
	    flexWrap: 'wrap',
	},
	
	Screen: {
	    width: '100%',
		height: '95%',
	},

});