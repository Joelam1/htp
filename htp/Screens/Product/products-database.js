import { Actions } from 'react-native-router-flux';
import {
    StyleSheet,
    Dimensions,
} from 'react-native';

import * as _ from 'lodash';

const deviceHeight = Dimensions.get("window").height;

class ProductProperty {
    constructor(property, url) {
        this.property = property;
        this.url = url;
    }
}
//Space Heater Commercial
const commercialSpaceHeaterCategory = {
    name: 'Commercial Space',
    id:'com-space-heating',
    image: require('../../img/Commercial.jpg'),
    breadcrumbs: [
        {
            text: 'Com Or Res',
            navigate: () => Actions.push('ProductCommercialOrResidential', {
                commercialCategory: commercialSpaceHeaterCategory,
                residentialCategory: residentialSpaceHeaterCategory,
            })
        },
        {
            text: 'Commercial Space',
            navigate: () => Actions.push('ProductCategory', { productCategory: commercialSpaceHeaterCategory })
        }
    ],
    products: [
        {
            //Commercial Elite FT
            name: 'Commercial Elite FT',
            id: 'commercial-Elite-FT',
            image: require('../../img/SpaceHeating/Residential/Elite-FT.jpg'),
            model: ['EFT-285 ','EFT-399'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EliteFT-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=pIVuvOrIRVU'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-387.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EliteFTInstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EliteFTParts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-391.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-659.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/EFT.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-387.pdf#limitedwarranty')
            ]
        },
        {
            //Commercial Elite Premier
            name: 'Commercial Elite Premier',
            id: 'commercial-Elite-Premier',
            image: require('../../img/SpaceHeating/Residential/Elite-PREMIER.jpg'),
            model:['EP-80 ','EP-110 ','EP-150 ','EP-220 ','EP-299 ','EP-301 ','EP-399 '],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-premier-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=S81DE4yKcTs'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-550.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EP_InstallationDrawing'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EP_PartsDrawing'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-558.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-549.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/ElitePremier.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-550.pdf#limitedwarranty')
            ]
        },
        {
            //Elite XL
            name: 'Elite XL',
            id: 'Elite-XL',
            image: require('../../img/SpaceHeating/Commercial/Elite-XL.jpg'),
            model: ['ELX-400FBN ', 'ELX-500FBN ','ELX-650FBN ','ELX-800FBN ','ELX-1000FBN ','ELX-2000FBN'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-xl-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=2bIXGqiBWJw'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-666.pdf#generalsafetyinformation'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-690.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-691.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-666.pdf#limitedwarranty')
            ]
        },
        {
            //EnduroTI
            name: 'EnduroTI',
            id: 'EnduroTI',
            image: require('../../img/SpaceHeating/Commercial/EnduroTI.jpg'),
            model: ['ETI-750 ','ETI-1000'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EnduroTi-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=6eEhQxn4UO0'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-517.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EnduroTi_InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EnduroTi_PartsDrawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-518.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-560.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-546.pdf')
            ]
        },
        {
            //Mod Con
            name: 'Mod Con',
            id: 'Mod-Con',
            image: require('../../img/SpaceHeating/Commercial/Mod-Con.jpg'),
            model:['ModCon500 ','ModCon700 ','ModCon850'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/ModCon-Brochure.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/ModCon.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-445-r4.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/modcon-installation-images.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/modconparts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-247.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-466.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-445-r4.pdf#limitedwarranty')
            ]
        },
        {
            //Mod Con Double Stack
            name: 'Mod Con Double Stack',
            id: 'Mod-Con-DS',
            image: require('../../img/SpaceHeating/Commercial/Mod-Con-DS.jpg'),
            model:['ModConDoubleStack-1000 ','ModConDoubleStack-1700 '],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/MKTLIT-42.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=UgBkM0Fv2Dk'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-428-r1.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/modconds-installation-images.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/modconds-parts-drawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-427.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-426_sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-428-r1.pdf#limitedwarranty')
            ]
        },
        {
            //Commercial UFT
            name: 'Commercial UFT',
            id: 'commercial-UFT',
            image: require('../../img/SpaceHeating/Residential/UFT.jpg'),
            model:['UFT-80 ','UFT-100 ','UFT-120 ','UFT-140 ','UFT-175 ','UFT-199'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/UFTWallFloor-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=9-c06u7GQjU'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-542.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-543.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/lp-542.pdf#waterpiping'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/lp-542.pdf#replacementparts'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-554.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-555.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-542.pdf#limitedwarranty')
            ]
        },
    ],
}

//Space Heater Residential
const residentialSpaceHeaterCategory = {
    name: 'Residential Space',
    id:'res-space-heating',
    image: require('../../img/Residential.jpg'),
    breadcrumbs: [
        {
            text: 'Com Or Res',
            navigate: () => Actions.push('ProductCommercialOrResidential', {
                commercialCategory: commercialSpaceHeaterCategory,
                residentialCategory: residentialSpaceHeaterCategory,
            })
        }, {
            text: 'Residential Space',
            navigate: () => Actions.push('ProductCategory', { productCategory: residentialSpaceHeaterCategory })
        }
    ],
    products: [
        {
            //Residential Elite FT
            name: 'Residential Elite FT',
            id: 'residential-Elite-FT',
            image: require('../../img/SpaceHeating/Residential/Elite-FT.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EliteFT-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=pIVuvOrIRVU'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-387.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EliteFTInstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EliteFTParts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-391.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-659.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/EFT.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-387.pdf#limitedwarranty')
            ]
        },
        {
            //Residential Elite Premier
            name: 'Residential Elite Premier',
            id: 'residential-Elite-Premier',
            image: require('../../img/SpaceHeating/Residential/Elite-PREMIER.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-premier-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=S81DE4yKcTs'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-550.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EP_InstallationDrawing'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EP_PartsDrawing'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-558.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-549.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/ElitePremier.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-550.pdf#limitedwarranty')
            ]
        },
        {
            //Elite Ultra
            name: 'Elite Ultra',
            id: 'Elite-Ultra',
            image: require('../../img/SpaceHeating/Commercial/Elite-Ultra.jpg'),
            model:['ELU-85WBN',' ELU-120WBN',' ELU-120WCN',' ELU-150WCN'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-ultra-brochure.pdf'),
                new ProductProperty('OverView Video','http://www.youtube.com/watch?v=OZLooM2hS20'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-700.pdf'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/420010970100.pdf'),
                new ProductProperty('Quick Installation Guide', 'http://www.htproducts.com/literature/420010970500.pdf'),
                new ProductProperty('Replacement Parts', 'http://www.htproducts.com/literature/R8218344-01.pdf'),
                new ProductProperty('Heating Boiler Specification', 'http://www.htproducts.com/literature/lp-713.docx'),
                new ProductProperty('Heating Boiler Submittal', 'http://www.htproducts.com/literature/lp-714.pdf'),
                new ProductProperty('Combi Boiler Specification', 'http://www.htproducts.com/literature/lp-717.docx'),
                new ProductProperty('Combi Boiler Submittal', 'http://www.htproducts.com/literature/lp-718.pdf'),
                new ProductProperty('Wall Mounting Template','http://www.htproducts.com/literature/420020189000_stampa.pdf'),
                new ProductProperty('HTP Link Connect Quick Start Guide', 'http://www.htproducts.com/literature/420011057800.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-707.pdf')
            ]
        },
        {
            //Pioneer
            name: 'Pioneer',
            id: 'Pioneer',
            image: require('../../img/SpaceHeating/Residential/PIONEER.jpg'),
            model:['PHR-100-55 ','PHR-130-55 ','PHR-160-55 ','PHR-199-55'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/pioneerbrochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=CVfSUlkai-8'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-325_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/pioneer-installation-images.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/pioneerparts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-349.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-348.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-325_0417.pdf#limitedwarranty')
            ]
        },
        {
            //Residential UFT
            name: 'Residential UFT',
            id: 'residential-UFT',
            image: require('../../img/SpaceHeating/Residential/UFT.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/UFTWallFloor-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=9-c06u7GQjU'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-542.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-543.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/lp-542.pdf#waterpiping'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/lp-542.pdf#replacementparts'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-554.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-555.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-542.pdf#limitedwarranty')
            ]
        },
    ],
}

//Combination Appliance
const comboApplianceCategory = {
    id: 'combo-appliance',
    name: 'Combination Appliance',
    image: require('../../img/Products/CombinationAppliances.jpg'),
    breadcrumbs: [{
        text: 'Combination Appliance',
        navigate: () => Actions.push('ProductCategory', { productCategory: comboApplianceCategory })
    }],
    products: [
        {
            //UFTC
            name: 'UFTC',
            id: 'Combination-UFTC',
            image: require('../../img/Combination/UFTC.jpg'),
            model:['UFTC-140 ','UFTC-199'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/UFTC-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=wP0BDB_NRog&list=PLmfVC1MlriuXitGWqHGuo2c0kG_hEs8Vb&index=2'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-648.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/UFTC_InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/UFTC-PartsDiagram.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-645.doc'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-646.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-648.pdf#limitedwarranty')
            ]
        },
        {
            //Combination Phoenix
            name: 'Combination Phoenix',
            id: 'CCPhoenix',
            image:require('../../img/Combination/Phoenix.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-191i.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuWPXNWQrE2DDs14hSwqRsi_&v=TNrU2v0PBdE'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/Phoenix-InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/Phoenix-PartsDrawing.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-193.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-179_0417.pdf#limitedwarranty'),
            ]
        },
        {
            //Combination Phoenix Light Duty
            name: 'Combination Phoenix Light Duty',
            id: 'Commercial-Phoenix-Light-Duty',
            image: require('../../img/Combination/PhoenixLD.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Phoenix-LD-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuXQIfIl5-5CJ47pK6yV7EQ8&v=JbjLvYvXAVg'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-441.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/Phoenixldinstall.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/phoenixld-parts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-456.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-457.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/PhoenixLD.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-441.pdf#limitedwarranty'),
            ]
        },
        {
            //Combination Elite Ultra
            name: 'Combination Elite Ultra',
            id: 'Combination-Elite-Ultra',
            image: require('../../img/SpaceHeating/Commercial/Elite-Ultra.jpg'),
            model:[''],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-ultra-brochure.pdf'),
                new ProductProperty('Installation Manual','http://www.htproducts.com/literature/lp-700.pdf'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/420010970100.pdf'),
                new ProductProperty('Quick Installation Guide', 'http://www.htproducts.com/literature/420010970500.pdf'),
                new ProductProperty('Replacement Parts', 'http://www.htproducts.com/literature/R8218344-01.pdf'),
                new ProductProperty('Heating Boiler Specification', 'http://www.htproducts.com/literature/lp-713.docx'),
                new ProductProperty('Heating Boiler Submittal', 'http://www.htproducts.com/literature/lp-714.pdf'),
                new ProductProperty('Combi Boiler Specification', 'http://www.htproducts.com/literature/lp-717.docx'),
                new ProductProperty('Combi Boiler Submittal', 'http://www.htproducts.com/literature/lp-718.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-707.pdf')
            ]
        },
        {
            //Versa Hydro
            name: 'Versa Hydro',
            id: 'VersaHydro',
            image: require('../../img/Combination/VersaHydro.jpg'),
            model:['PHE130-55 ','PHE130-80 ','PHE130-119 ','PHE199-55 ','PHE199-80 ','PHE199-119'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/versahydrobrochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=cfuffLjfqeY&list=PLmfVC1MlriuVGMXCh04FBVG3QQlJA3SsX'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-314_0417.pdf#generalsafetyinformation'),
                new ProductProperty('User Manuals', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-329.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-328.sub.pdf'),
                new ProductProperty('Installation Drawings', 'http://www.htproducts.com/literature/VersaHydroInstall.pdf'),
                new ProductProperty('Parts Drawings', 'http://www.htproducts.com/literature/VersaHydroParts.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-314_0417.pdf#limitedwarranty'),
            ]
        },
        {
            //Versa Hydro Solar
            name: 'Versa Hydro Solar',
            id: 'VersaHydroSolar',
            image: require('../../img/Combination/VersaHydroSolar.jpg'),
            model:['PHE130-80S ','PHE130-119S ','PHE199-80S ','PHE199-119S'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/versahydrobrochure.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-326.pdf'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-204.pdf#generalsafetyinformation'),
                new ProductProperty('User Manuals', 'http://www.htproducts.com/literature/lp-314.pdf'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-329.pdf'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-363.sub.pdf'),
                new ProductProperty('Installation Drawings', 'http://www.htproducts.com/literature/versahydro-installation-images.pdf'),
                new ProductProperty('Parts Drawings', 'http://www.htproducts.com/literature/versahydroparts.pdf'),
                new ProductProperty('Solar Kit Instructions', 'http://www.htproducts.com/literature/lp-420.pdf')
            ]
        }
    ],
}

//Indirect Water Heater
const indirectWaterCategory = {
    name: 'Indirect Water Heating',
    id:"indirect-water-heating",
    image: require('../../img/Products/IndirectWaterHeating.jpg'),
    breadcrumbs: [{
        text: 'Indirect Water Heating',
        navigate: () => Actions.push('ProductCategory', { productCategory: indirectWaterCategory })
    }],
    products: [
        {
            //SuperStor Contender
            name: 'SSContender',
            id: 'SuperStor-Contender',
            image: require('../../img/Indirect/SuperStorContender.jpg'),
            model:['SSC-35 ','SSC-50 ','SSC-80 ','SSC-119'],
            properties:[
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-66.pdf'),
                new ProductProperty('Use and Care Manual', 'http://www.htproducts.com/literature/lp-65.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/superstorcontender-installation-images.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/SScontender-PartsDrawing.pdf'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-502.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-65.pdf#limitedwarranty'),
            ]
        },
        {
            //SuperStor Ultra
            name: 'SuperStor Ultra',
            id: 'SuperStor-Ultra',
            image : require('../../img/Indirect/SuperStorUltra.jpg'),
            model:['SSU-20 ','SSU-30 ','SSU-30LB ','SSU-45 ','SSU-60 ','SSU-60C ','SSU-80 ','SSU-80C ','SSU-119'],
            properties:[
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-81.pdf'),
                new ProductProperty('Installation Manual mfg On/After 01/01/2020','http://www.htproducts.com/literature/lp-730.pdf'),
                new ProductProperty('Installation Manual mfg Before 01/01/2020', 'http://www.htproducts.com/literature/lp-83.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://htproducts.com/literature/superstorulta-installation-drawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/superstorultra-parts-images.pdf'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-252.sub.pdf'),
                new ProductProperty('Buffer Tank Installation Manual', 'http://www.htproducts.com/literature/LP-219.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-730.pdf#limitedwarranty'),
            ]
        },
        {
            //SuperStor Pro
            name: 'SuperStor Pro',
            id:'SuperStor-Pro',
            image : require('../../img/Indirect/SuperStor-Pro.jpg'),
            model:['SSP-30 ','SSP-40 ','SSP-50 ','SSP-60 ','SSP-80 ','SSP-100'],
            properties:[
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/SuperStor_Pro_Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=4hJ5bVqnVNY'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-451.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://htproducts.com/literature/SSP_installationimages'),
                new ProductProperty('Parts Drawing', 'http://htproducts.com/literature/SSP_partsdrawing.pdf'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-453.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-451.pdf#limitedwarranty'),
            ]
        }, 
        {
            //SuperStor Pro Pool
            name: 'SuperStor Pro Pool',
            id:'SuperStor-ProPool',
            image : require('../../img/Indirect/SuperStor-ProPool.jpg'),
            model:['SSP-20PH'],
            properties:[
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-21.pdf#generalsafetyinformation'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-21.pdf#limitedwarranty'),
            ]
        }, 
    ]
}

//Solar
const solarCategory = {
    name: 'Solar',
    id: 'solar',
    image: require('../../img/Products/Solar.jpg'),
    breadcrumbs: [{
        text: 'Solar',
        navigate: () => Actions.push('ProductCategory', { productCategory: solarCategory })
    }],
    products: [
        {
            //Phoenix Solar
            name: 'Phoenix Solar',
            id: 'PhoenixSolar',
            model:['PH130-80S ','PH199-80S ','PH130-119S ','PH199-119S'],
            styles: {
                Button: {
                    height: deviceHeight/8
                }
            },
            image: require('../../img/Solar/Phoenix-Solar.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/SolarBrochure.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-204.pdf#limitedwarranty'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-204.pdf#generalsafetyinformation'),
                new ProductProperty('User Manuals', 'http://www.htproducts.com/literature/lp-204.pdf'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-193.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-201.sub.pdf'),
                new ProductProperty('Installation Drawings', 'http://www.htproducts.com/literature/lp-179_04717-installation.pdf'),
                new ProductProperty('Parts Drawings', 'http://www.htproducts.com/literature/lp-179_04717-parts.pdf'),
            ]
        }, 
        {
            //Light Duty Solar
            name: 'Light Duty Solar', 
            id: 'Light-Duty-Solar',
            image: require('../../img/Solar/PLiteDSolar.jpg'),
            model:['PH76-80S'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Phoenix-LD-Brochure.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-204.pdf#limitedwarranty'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-204.pdf#generalsafetyinformation'),
                new ProductProperty('User Manuals', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-456.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-600.pdf'),
                new ProductProperty('Installation Drawings', 'http://www.htproducts.com/Phoenixldinstall.pdf'),
                new ProductProperty('Parts Drawings', 'http://www.htproducts.com/literature/phoenixld-parts.pdf'),
                new ProductProperty('Solar Installation Supplements', 'http://www.htproducts.com/literature/lp-204.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/PhoenixLD.pdf')
            ]
        }, 
        {
            //DrainBack
            name: 'DrainBack',
            id: 'DrainBack',
            image: require('../../img/Solar/Solar-DrainBack-Tank.jpg'),
            model:['SSU-10DB ','SSU-15DB ','SSU-20DB ','SSU-30DB ','SSU-40DB ','SSU-60DB ','SSU-80DB'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/SolarBrochure.pdf'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-585.pdf#generalsafetyinformation'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-267.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-284.pdf'),
            ]
        }, 
        {
            //SuperSolar
            name: 'SuperSolar',
            id: 'SuperSolar',
            image: require('../../img/Solar/SuperStorSolar.jpg'),
            model:['SSU-60SB ','SSU-80SB ','SSU-119SB'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/SolarBrochure.pdf'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-199.pdf#generalsafetyinformation'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-196.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-195.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-82.pdf')
            ]
        }, 
        {
            //SuperStor Pro
            name: 'SuperProSol',
            id: 'SuperProSol',
            image: require('../../img/Solar/SuperStor-Pro.jpg'),
            model:['SSP-80SE ','SSP-115SE'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/SuperStor_Pro_Brochure.pdf'),
                new ProductProperty('Installation Manuals', 'http://www.htproducts.com/literature/lp-451.pdf#generalsafetyinformation'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-622.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-453.pdf'),
                new ProductProperty('Installation Drawings', 'http://htproducts.com/literature/SSP_installationimages'),
                new ProductProperty('Parts Drawings', 'http://htproducts.com/literature/SSP_partsdrawing.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-451.pdf#limitedwarranty')
            ]
        }, 
        {
            //Versa Hydro Solar
            name: 'HydroSolar',
            id: 'HydroSolar',
            image: require('../../img/Solar/VersaHydroSolar.jpg'),
            model:['PHE130-80S ','PHE199-80S ','PHE130-119S ','PHE199-119S'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/versahydrobrochure.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-326.pdf'),
                new ProductProperty('User Manuals', 'http://www.htproducts.com/literature/lp-314.pdf'),
                new ProductProperty('Installation Manuals', 'General Safety Information','http://www.htproducts.com/literature/lp-204.pdf#generalsafetyinformation'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-329.pdf'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-363.sub.pdf'),
                new ProductProperty('Installation Drawings', 'http://www.htproducts.com/literature/versahydro-installation-images.pdf'),
                new ProductProperty('Parts Drawings', 'http://www.htproducts.com/literature/versahydroparts.pdf'),
                new ProductProperty('Solar Kit Instructions', 'http://www.htproducts.com/literature/lp-420.pdf'),
            ]
        }, 
        {
            name: 'Large Volume Solar',
            id: 'Large-Volume-Solar',
            image: require('../../img/Solar/LVSolarTanks.jpg'),
            properties: [
                {
                    property: 'Brochure',
                    url: 'http://www.htproducts.com/literature/Large-Volume-Solar-Tanks.pdf'
                }
            ]
                        
        }, 
    ]
}

//Comfort Solutions
const comfortSolutionProducts = [
    {
        name: 'Comfort Solution',
        id: 'comfort-solution',
        image: require('../../img/Products/FanCoil-Header.jpg'),
        model:['VFC-9PWV ','VFC-14PWV ','VFC-20PWV ','VFC-25PWV ','VFC-32PWV ','VFC-9GB ','VFC-14GB ','VFC-20GB ','VFC-25GB ','VFC-32GB'],
        properties: [
            new ProductProperty('Brochure', 'http://www.htproducts.com/literature/UltraThin-Hydronic-FanCoil.pdf'),
            new ProductProperty('Specification & Submittal', 'http://www.htproducts.com/literature/lp-610.pdf'),
            new ProductProperty('Metal Face Installation Manual', 'http://www.htproducts.com/literature/lp-586.pdf#generalsafetyinformation'),
            new ProductProperty('Glass Face Installation Manual', 'http://www.htproducts.com/literature/lp-587.pdf#generalsafetyinformation'),
            new ProductProperty('Metal Face Warranty', 'http://www.htproducts.com/literature/lp-586.pdf#limitedwarranty'),
            new ProductProperty('Glass Face Warranty', 'http://www.htproducts.com/literature/lp-587.pdf#limitedwarranty'),
        ]
    }
]

// COMMERCIAL WATER HEATER CATEGORIES
const commercialWaterHeaterCategory = {
    name: 'Commercial',
    id:'com-water-tank-tankless',
    image: require('../../img/Commercial.jpg'),
    navigate: () => Actions.push('ProductCategory', {
        productCategory: TankTanklessCategory,
    }),
    breadcrumbs: [
        {
            text: 'Com Or Res',
            navigate: () => Actions.push('ProductCommercialOrResidential', {
                commercialCategory: commercialWaterHeaterCategory,
                residentialCategory: residentialWaterHeaterCategory,
            })
        }, {
            text: 'Commercial Tank or Tankless',
            navigate: () => Actions.push('ProductCategory', { productCategory: commercialWaterHeaterCategory })
        }
    ],
    products: [{
        name: 'Tank',
        id: 'tank',
        image: require('../../img/WaterHeating/Tank.jpg'),
        navigation: () => Actions.push('ProductCategory', {
            productCategory: commercialTankGasOrElectricCategory
        })
    }, {
        name: 'Tankless',
        id: 'tank-less',
        image: require('../../img/WaterHeating/Tankless.jpg'),
        navigation: () => Actions.push('ProductCategory', {
            productCategory: commercialTanklessGasOrElectricCategory
        })
    }, {
        name: 'Supply Boiler',
        id: 'supply-boiler',
        image: require('../../img/WaterHeating/SupplyBoiler.jpg'),
        navigation: () => Actions.push('ProductCategory', {
            productCategory: supplyBoilerCategory
        })
    }],
}
const supplyBoilerCategory = {
    name:'Supply Boiler',
    id:'supply-boiler-category',
    image: require('../../img/WaterHeating/SupplyBoiler.jpg'),
    breadcrumbs: [...commercialWaterHeaterCategory.breadcrumbs, {
        text: 'Supply Boiler',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: supplyBoilerCategory
        })
    }],

    products:[
        {
            name:'Elite Premier VWH',
            id:'elite-premier-vwh',
            image: require('../../img/WaterHeating/elite-vwh.jpg'),
            model:['EP-220VWH ','EP-290VWH ','EP-301VWH ','EP-399VWH'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Elite-Premier-VWH-brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-551.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/elite-premier-vwh-install.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/elitevwhparts.pdf'),
                new ProductProperty('Specification', 'http://htproducts.com/literature/lp-598.docx'),
                new ProductProperty('Submittal', 'http://htproducts.com/literature/lp-599.pdf'),
                new ProductProperty('Buy American', 'http://htproducts.com/literature/Buy-American/ElitePremierVWH.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-551.pdf#limitedwarranty'),
            ]
        },
        {
            name:'Elite XL VWH',
            id:'elite-xl-vwh',
            image: require('../../img/WaterHeating/EliteXL-VWH.jpg'),
            model:['ELX-400FVWHN ','ELX-500FVWHN ','ELX-650FVWHN ','ELX-800FVWHN ','ELX-1000FVWHN ','ELX-1500FVWHN ','ELX-2000FVWHN'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/elite-xl-vwh-brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-701.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EliteXL-VWH-Parts-Drawing.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-692.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-693.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/2019-BuyAmerican-EliteXL-VWH.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-701.pdf#limitedwarranty'),
            ]
        },
        {
            name:'Mod Con VWH',
            id:'ModCon-vwh',
            image: require('../../img/WaterHeating/modcon-vwh.jpg'),
            model:['ModCon500VWH ','ModCon700VWH ','ModCon850VWH'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-277.pdf'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-446-r4.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/ModConVWHInstall.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/ModConVWHParts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-295.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-467.sub.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/ModConVWH.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-446-r4.pdf#limitedwarranty'),
            ]
        },
        {
            name:'Mod Con DS VWH',
            id:'ModConDS-vwh',
            image: require('../../img/WaterHeating/modconDS-vwh.jpg'),
            model:['ModConDS1000 ','ModConDS1700'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/ModCon-Double-Stack-VWH-Brochure.pdf'),
                new ProductProperty('User Manual','http://www.htproducts.com/literature/lp-447.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-429-r1.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/ModConDSVWHInstall.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/ModConDSVWHParts'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-431.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-430_sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-429-r1.pdf#limitedwarranty'),
            ]
        },
    ]
}
//Commercial Tank
const commercialTankGasOrElectricCategory = {
    name: 'Gas Or Electric',
    id: 'commercial-tank-gas-electric',
    image: require('../../img/Commercial.jpg'),
    breadcrumbs: [...commercialWaterHeaterCategory.breadcrumbs, {
        text: 'Gas Or Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTankGasOrElectricCategory
        })
    }],
    products: [
        {
            name: 'Gas',
            //Commercial Tank Gas
            id: 'CTG',
            image: require('../../img/WaterHeating/Gas.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: commercialTankGasCategory
            })
        },
        {
            name: 'Electric',
            //Commercial Tank Electric
            id: 'CTE',
            image: require('../../img/WaterHeating/Electric.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: commercialTankElectricCategory
            })
        }
    ]
}
//Commercial Tankless
const commercialTanklessGasOrElectricCategory = {
    name: 'Gas Or Electric',
    id: 'commercial-tankless-gas-electric',
    image: require('../../img/Commercial.jpg'),
    breadcrumbs: [...commercialWaterHeaterCategory.breadcrumbs, {
        text: 'Gas Or Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTanklessGasOrElectricCategory
        })
    }],
    products: [
        {
            name: 'Gas',
            //Commercial Tankless Gas
            id: 'CTLG',
            image: require('../../img/WaterHeating/Gas.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: commercialTanklessGasCategory
            })
        },
        {
            name: 'Electric',
            //Commercial Tankless Electric
            id: 'CTLE',
            image: require('../../img/WaterHeating/Electric-Tankless.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: commercialTanklessElectricCategory
            })
        }
    ]
}
//Water Heater Commercial Tank Gas
const commercialTankGasCategory = {
    name: 'Commercial Tank Gas',
    id: 'commercial-tank-gas',
    image: require('../../img/WaterHeating/Gas.jpg'),
    breadcrumbs: [...commercialTankGasOrElectricCategory.breadcrumbs, {
        text: 'Gas',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTankGasCategory
        })
    }],
    products: [
        {
            name: 'Commercial Phoenix',
            id: 'commercial-phoenix',
            image:require('../../img/WaterHeating/Phoenix.jpg'),
            model:'',
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-191i.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuWPXNWQrE2DDs14hSwqRsi_&v=TNrU2v0PBdE'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/Phoenix-InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/Phoenix-PartsDrawing.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-193.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-179_0417.pdf#limitedwarranty'),
            ]
        },
        {
            name: 'Multi-Fit',
            id: 'commercial-multi-fit',
            image: require('../../img/WaterHeating/PhoenixMultiFit.jpg'),
            model:['PHM130-80 ','PHM199-80 ','PHM130-100 ','PHM199-100'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Phoenix-Multi-Fit-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/PhoenixMultiFit_InstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/PhoenixMultiFit_PartsDrawings.pdf'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-104.pdf'),
            ]
        },
        {
            name: 'Phoenix Plus',
            id: 'Phoenix-Plus',
            image: require('../../img/WaterHeating/PhoenixPlus.jpg'),
            model:['PHP199-119 ','PHP260-119 ','PHP320-119 ','PHP399-119'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/MKTLIT-52.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-454_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/PhoenixPlusInstall.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/PhoenixPlusParts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-461.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-462.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=OGnN42Q2MVw'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-454_0417.pdf#limitedwarranty'),
            ]
        },

        {
            name: 'Phoenix Sanitizer',
            id: 'Phoenix-Sanitizer',
            image: require('../../img/WaterHeating/PhoenixSanitizer.jpg'),
            model:['PH130-55SA ','PH199-55SA'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-191i.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuWPXNWQrE2DDs14hSwqRsi_&v=TNrU2v0PBdE'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/Phoenix-InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/Phoenix-PartsDrawing.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-193.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-179_0417.pdf#limitedwarranty'),
            ]
        },
    ]
}

//Water Heater Commercial Tank Electric
const commercialTankElectricCategory = {
    name: 'Commercial Tank Electric',
    id: 'commercial-tank-electric',
    image: require('../../img/WaterHeating/Electric.jpg'),
    breadcrumbs: [...commercialTankGasOrElectricCategory.breadcrumbs, {
        text: 'Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTankElectricCategory
        })
    }],
    products: [
        {
            name: 'Commercial Everlast Mini Tank',
            id: 'Everlast-MiniTank',
            image:require('../../img/WaterHeating/EverlastMiniTank.jpg'),
            model:['EVR02.5A014C ','EVR04.0A014C ','EVR08.0A020C'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVMiniTankBrochure.pdf'),
                new ProductProperty('User & Care Manual', 'http://www.htproducts.com/literature/420010915300.pdf#generalsafetyinformation'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-696.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/420010915300.pdf#limitedwarranty'),
            ]
        },
        {
            name: 'Commercial Point of Use',
            id: 'Commercial-PointOfUse',
            image:require('../../img/WaterHeating/CPointofUse.jpg'),
            model:['EVC010C1X045 ','EVC015C1X045 ','EVC020C1X045'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVC-Point-of-Use-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-661.pdf#generalsafetyinformation'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-663.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-661.pdf#limitedwarranty'),
            ]
        },
        {
            name: 'Everlast 3 Elements',
            id: 'Everlast-ThreeElements',
            image:require('../../img/WaterHeating/ThreeElements.jpg'),
            model:['EVC080C3W135 ','EVC080C3W165 ','EVC080D3W180 ','EVC080E3W180 ','EVC100C3W135 ','EVC100C3W165 ','EVC100D3W180 ','EVC100E3W180 ','EVC115C3W135 ','EVC115C3W165 ','EVC115D3W180 ','EVC115E3W180'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Everlast-MD-Commercial-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-647.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EVC3_InstallationDiagram.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EVC3_PartsDrawing.pdf'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-652.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/2018-BuyAmerican-MediumDutyComm3Element.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-647.pdf#limitedwarranty'),
            ]
        },
        {
            name: 'Everlast Light Duty',
            id: 'commercial-Everlast-light-dutys',
            image:require('../../img/WaterHeating/LightDuty.jpg'),
            model:['EVC080C2X045 ','EVC080C2X055 ','EVC100C2X045 ','EVC100C2X055 ','EVC115C2X045 ','EVC115C2X055'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVLD-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-565.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing','http://www.htproducts.com/literature/everlast-light-com-installation-drawings.pdf'),
                new ProductProperty('Parts Drawing','http://www.htproducts.com/literature/everlast-light-com-parts-drawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-564.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-563.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-565.pdf#limitedwarranty'),
            ]
        },
        {
            name: 'Everlast Medium Duty',
            id: 'Medium Duty',
            image:require('../../img/WaterHeating/MediumDuty.jpg'),
            model:['EVC030C2T045 ','EVC040C2T045 ','EVC050C2T045'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVC-MD-Brochure-3Ph.pdf'),
                new ProductProperty('Use and Care Manual', 'http://www.htproducts.com/literature/lp-660.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-660.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EverlastComm-InstallDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EverlastComm-PartsDrawing.pdf'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-662.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/2018-BuyAmerican-MediumDutyCommercial.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-425.pdf'),
            ]
        },
        {
            name: 'Everlast Grid-Enabled',
            id: 'GridEnabled',
            image:require('../../img/WaterHeating/GridEnabled.jpg'),
            model:['EVG080C2X045H ','EVG080C2X055H ','EVG100C2X045H ','EVG100C2X055H ','EVG115C2X045H ','EVG115C2X055H'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVG-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-608_0618.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/EVG-InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/EVG-Parts.pdf'),
                new ProductProperty('Spec & Submittal', 'http://htproducts.com/literature/lp-633.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-608_0618.pdf#limitedwarranty'),
            ]
        },
    ]
}
//Water Heater Commercial Tankless Gas
const commercialTanklessGasCategory = {
    name: 'Commercial Tankless Gas',
    id: 'commercial-tankless-gas',
    image: require('../../img/WaterHeating/Gas.jpg'),
    breadcrumbs: [...commercialTanklessGasOrElectricCategory.breadcrumbs, {
        text: 'Gas',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTanklessGasCategory
        })
    }],
    products: [
        //Hydra Smart RTC
        {
            name: 'Hydra Smart RTC',
            id: 'RTC',
            image:require('../../img/WaterHeating/HydraSmartRTC.jpg'),
            model:['RTC-199 '],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/HydraSmartRTC-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=yKpSUNmldfs'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-538.pdf#generalsafetyinformation'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-504.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/RTC_InstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/RTC_PartsDrawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-575.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-576.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-538.pdf#limitedwarranty'),
            ]
        },
        //Cross
        {
            name: 'Crossover Commercial',
            id: 'Crossover-Commercial',
            image:require('../../img/WaterHeating/CrossoverCommercial.jpg'),
            model:['CGH-199W ','CGH-199WO ','CGH-199FO ','CGH-199WLP ','CGH-199WOLP ','CGH-199FOLP '],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Crossover-Commercial-brochure.pdf'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-644.pdf#generalsafetyinformation'),
                new ProductProperty('Outdoor Installation Manual', 'http://www.htproducts.com/literature/lp-673.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/CGH-InstallDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/CGH-PartsDrawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-650.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-649.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-644.pdf#limitedwarranty'),
            ]
        },
    ]
}
//Water Heater Commercial Tankless Electric
const commercialTanklessElectricCategory = {
    name: 'Commercial Tankless Electric',
    id: 'commercial-tankless-Electric',
    image: require('../../img/WaterHeating/Electric.jpg'),
    breadcrumbs: [...commercialTanklessGasOrElectricCategory.breadcrumbs, {
        text: 'Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: commercialTanklessElectricCategory
        })
    }],
    products: [
        //Everlast Whole Home
        {
            name: 'Everlast Whole Home',
            id: 'EWH',
            image:require('../../img/WaterHeating/Everlast-Whole-Home-Electric-Tankless.jpg'),
            model:['EVIWH 18-240',' EVIWH 27-240',' EVIWH 36-240'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/everlast-electric-tankless-brochure.pdf'),
                new ProductProperty('Use and Care Manual', 'http://www.htproducts.com/literature/198857.pdf'),
                new ProductProperty('Spec and Submittals', 'http://www.htproducts.com/literature/lp-705.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/198857.pdf#limitedwarranty'),
            ]
        },
    ]
}
//RESIDENTIAL WATER HEATER CATEGORIES
const residentialWaterHeaterCategory = {
    name: 'Residential',
    id:'residential-water-heating',
    image: require('../../img/Residential.jpg'),
    navigate: () => Actions.push('ProductCategory', {
        productCategory: TankTanklessCategory,
    }),
    breadcrumbs: [
        {
            text: 'Com Or Res',
            navigate: () => Actions.push('ProductCommercialOrResidential', {
                commercialCategory: commercialWaterHeaterCategory,
                residentialCategory: residentialWaterHeaterCategory,
            })
        }, {
            text: 'Residential Space',
            navigate: () => Actions.push('ProductCategory', { productCategory: residentialWaterHeaterCategory })
        }
    ],
    products: [{
        name: 'Tank',
        id: 'tank',
        image: require('../../img/WaterHeating/Tank.jpg'),
        navigation: () => Actions.push('ProductCategory', {
            productCategory: residentialTankGasOrElectricCategory
        })
    }, {
        name: 'Tankless',
        id: 'tank-less',
        image: require('../../img/WaterHeating/Tankless.jpg'),
        navigation: () => Actions.push('ProductCategory', {
            productCategory: residentialTanklessGasOrElectricCategory
        })
    }],
}
//Residential Tank
const residentialTankGasOrElectricCategory = {
    name: 'Gas Or Electric',
    id: 'residential-tank-gas-electric',
    image: require('../../img/Residential.jpg'),
    breadcrumbs: [...residentialWaterHeaterCategory.breadcrumbs, {
        text: 'Gas Or Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTankGasOrElectricCategory
        })
    }],
    products: [
        {
            name: 'Gas',
            //residential Tank Gas
            id: 'RTG',
            image: require('../../img/WaterHeating/Gas.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: residentialTankGasCategory
            })
        },
        {
            name: 'Electric',
            //residential Tank Electric
            id: 'RTE',
            image: require('../../img/WaterHeating/Electric.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: residentialTankElectricCategory
            })
        }
    ]
}
//Residential Tankless
const residentialTanklessGasOrElectricCategory = {
    name: 'Gas Or Electric',
    id: 'residential-tankless-gas-electric',
    image: require('../../img/Residential.jpg'),
    breadcrumbs: [...residentialWaterHeaterCategory.breadcrumbs, {
        text: 'Gas Or Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTanklessGasOrElectricCategory
        })
    }],
    products: [
        {
            name: 'Gas',
            //residential Tankless Gas
            id: 'RTLG',
            image: require('../../img/WaterHeating/Gas.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: residentialTanklessGasCategory
            })
        },
        {
            name: 'Electric',
            //residential Tankless Electric
            id: 'RTLE',
            image: require('../../img/WaterHeating/Electric-Tankless.jpg'),
            properties: [],
            navigation: () => Actions.push('ProductCategory', {
                productCategory: residentialTanklessElectricCategory
            })
        }
    ]
}
//Residential Tank Gas
const residentialTankGasCategory = {
    name: 'Residential Tank Gas',
    id: 'residential-tank-gas',
    image: require('../../img/WaterHeating/Gas.jpg'),
    breadcrumbs: [...residentialTankGasOrElectricCategory.breadcrumbs, {
        text: 'Gas',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTankGasCategory
        })
    }],
    products: [
        {
            name: 'Phoenix',
            id: 'residential-phoenix',
            image:require('../../img/WaterHeating/Phoenix.jpg'),
            model: ['PH100-55 ','PH130-55 ','PH160-55 ','PH199-55 ','PH100-80 ','PH130-80 ','PH160-80 ','PH199-80 ','PH100-119 ','PH130-119 ','PH160-119 ','PH199-119'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/lp-191i.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuWPXNWQrE2DDs14hSwqRsi_&v=TNrU2v0PBdE'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/Phoenix-InstallationDrawing.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/Phoenix-PartsDrawing.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-193.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-179_0417.pdf#limitedwarranty'),
            ]
        },

        {
            name: 'Residential PhoenixLD',
            id: 'PhoenixLD',
            image: require('../../img/WaterHeating/PhoenixLD.jpg'),
            model:['PH76-50 ','PH76-60 ','PH76-80'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Phoenix-LD-Brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?list=PLmfVC1MlriuXQIfIl5-5CJ47pK6yV7EQ8&v=JbjLvYvXAVg'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-441.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/Phoenixldinstall.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/phoenixld-parts.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-456.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-457.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/Buy-American/PhoenixLD.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-441.pdf#limitedwarranty'),
            ]
        },

        {
            name: 'Residential Multi-Fit',
            id: 'residential-multi-fit',
            image: require('../../img/WaterHeating/PhoenixMultiFit.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/Phoenix-Multi-Fit-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-179_0417.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/PhoenixMultiFit_InstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/PhoenixMultiFit_PartsDrawings.pdf'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-104.pdf'),
            ]
        },
        {
            name: 'Crossover Floor',
            id: 'crossover-floor',
            image: require('../../img/WaterHeating/CrossOverFloor.jpg'),
            model:['RGH20-75F ','RGH20-76F ','RGP20-100F'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/RGH75100-brochure.pdf'),
                new ProductProperty('Install Manual', 'http://www.htproducts.com/literature/lp-605_1019.pdf'),
                new ProductProperty('Manual Mfg After 9/4/18','http://www.htproducts.com/literature/lp-605_0918.pdf'),
                new ProductProperty('Manual Mfg Before 9/4/18','http://www.htproducts.com/literature/lp-605.pdf'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/RGH75100_InstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/RGH75100_InstallationDrawings.pdf'),
                new ProductProperty('Specifications', 'http://www.htproducts.com/literature/lp-611.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-354.sub.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/playlist?list=PLmfVC1MlriuUp54Evij5r4ck6X5fg_4-Z'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-605_0918.pdf#limitedwarranty'),
            ]
        },
    ]
}
//Residential Tank Electric
const residentialTankElectricCategory = {
    name: 'Residential Tank Electric',
    id: 'residential-tank-electric',
    image: require('../../img/WaterHeating/Electric.jpg'),
    breadcrumbs: [...residentialTankGasOrElectricCategory.breadcrumbs, {
        text: 'Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTankElectricCategory
        })
    }],
    products: [
        //Electric Mini Tank
        {
            name: 'Residential Everlast Mini Tank',
            id: 'residential-MiniTank',
            image:require('../../img/WaterHeating/EverlastMiniTank.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVMiniTankBrochure.pdf'),
                new ProductProperty('User & Care Manual', 'http://www.htproducts.com/literature/420010915300.pdf#generalsafetyinformation'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-696.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/420010915300.pdf#limitedwarranty'),
            ]
        },
        //Point of use
        {
            name: 'Residential Everlast Point of Use',
            id: 'residential-PointOfUse',
            image:require('../../img/WaterHeating/RPointofUse.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVR-Point-of-Use-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-668.pdf#generalsafetyinformation'),
                new ProductProperty('Spec & Submittal', 'http://www.htproducts.com/literature/lp-670.pdf'),
                new ProductProperty('Buy American', 'http://www.htproducts.com/literature/2018-BuyAmerican-ResidentialPOU.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-668.pdf#limitedwarranty'),
            ]
        },
        //Residential
        {
            name: 'Everlast Residential',
            id: 'everlast-residential',
            image:require('../../img/WaterHeating/Residential.jpg'),
            model:['EVR040C2X045 ','EVR040C2X055 ','EVR052C2X045 ','EVR052C2X055'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVR-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-631.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing','http://www.htproducts.com/literature/everlast-residential-installation-drawings.pdf'),
                new ProductProperty('Parts Drawing','http://www.htproducts.com/literature/everlast-residential-parts-drawings.pdf'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-478.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-631.pdf#limitedwarranty'),
            ]
        },
        //Light Duty
        {
            name: 'Residential Everlast Light Duty',
            id: 'residential-Everlast-light-duty',
            image:require('../../img/WaterHeating/LightDuty.jpg'),
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/EVLD-Brochure.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-565.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing','http://www.htproducts.com/literature/everlast-light-com-installation-drawings.pdf'),
                new ProductProperty('Parts Drawing','http://www.htproducts.com/literature/everlast-light-com-parts-drawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-564.docx'),
                new ProductProperty('Submittal', 'http://www.htproducts.com/literature/lp-563.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-565.pdf#limitedwarranty'),
            ]
        },
    ]
}
//Residential Tankless Gas
const residentialTanklessGasCategory = {
    name: 'Residential Tankless Gas',
    id: 'residential-tankless-gas',
    image: require('../../img/WaterHeating/Gas.jpg'),
    breadcrumbs: [...residentialTanklessGasOrElectricCategory.breadcrumbs, {
        text: 'Gas',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTanklessGasCategory
        })
    }],
    products: [
        //Hydra Smart RT
        {
            name: 'Hydra Smart RT',
            id: 'Hydra-Smart-RT',
            image:require('../../img/WaterHeating/HydraSmartRT.jpg'),
            model:['RT-150 ','RTO-150 ','RT-199 ','RTO-199'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/MKTLIT-61.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/watch?v=yKpSUNmldfs'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-538.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://www.htproducts.com/literature/RT_InstallationImages.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/RT_PartsDrawings.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-516.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-574.pdf'),
                new ProductProperty('Service Manual','http://www.htproducts.com/literature/lp-532.pdf'),
                new ProductProperty('Quick Reference Guide','http://www.htproducts.com/literature/lp-507.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-505.pdf#limitedwarranty'),
                new ProductProperty('Outdoor Installation Manual','http://www.htproducts.com/literature/lp-671.pdf'),
                new ProductProperty('Outdoor Installation Kit', 'http://htproducts.com/literature/lp-592.pdf'),
                new ProductProperty('Outdoor Warranty', 'http://www.htproducts.com/literature/lp-671.pdf#limitedwarranty'),
            ]
        },
        //Crossover Wall
        {
            name: 'Crossover Wall',
            id: 'crossover-wall',
            image:require('../../img/WaterHeating/CrossOverWall.jpg'),
            model:['RGH-150 ','RGH-150O ','RGH-199 ','RGH-199O'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/crossover-brochure.pdf'),
                new ProductProperty('Video', 'https://www.youtube.com/playlist?list=PLmfVC1MlriuUFhBpwmk-ioE0_AN54c_WG'),
                new ProductProperty('User Manual', 'http://www.htproducts.com/literature/lp-470.pdf'),
                new ProductProperty('Installation Manual', 'http://www.htproducts.com/literature/lp-644.pdf#generalsafetyinformation'),
                new ProductProperty('Installation Drawing', 'http://htproducts.com/literature/RGH_InstallationDrawings.pdf'),
                new ProductProperty('Parts Drawing', 'http://www.htproducts.com/literature/RGH_PartsDrawing120916.pdf'),
                new ProductProperty('Specification', 'http://www.htproducts.com/literature/lp-597.docx'),
                new ProductProperty('Submittals', 'http://www.htproducts.com/literature/lp-596.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/lp-590.pdf#limitedwarranty'),
                new ProductProperty('Outdoor Installation Manual','http://www.htproducts.com/literature/lp-672.pdf'),
                new ProductProperty('Outdoor Specification', 'http://www.htproducts.com/literature/lp-676.docx'),
                new ProductProperty('Outdoor Submittals', 'http://www.htproducts.com/literature/lp-677.pdf'),
                new ProductProperty('Outdoor Warranty', 'http://www.htproducts.com/literature/lp-672.pdf#limitedwarranty'),
            ]
        },
    ]
}

//Water Heater Residential Tankless Electric
const residentialTanklessElectricCategory = {
    name: 'Residential Tankless Electric',
    id: 'residential-tankless-Electric',
    image: require('../../img/WaterHeating/Electric.jpg'),
    breadcrumbs: [...residentialTanklessGasOrElectricCategory.breadcrumbs, {
        text: 'Electric',
        navigate: () => Actions.push('ProductCategory', {
            productCategory: residentialTanklessElectricCategory
        })
    }],
    products: [
        //Everlast Single Point
        {
            name: 'Everlast Single Point',
            id: 'ESP',
            image:require('../../img/WaterHeating/Everlast-Single-Electric-Tankless.jpg'),
            model:['EVISP 3-110 CP',' EVISP 6.5-240',' EVISP 10.5-240',' EVISP 13-240'],
            properties: [
                new ProductProperty('Brochure', 'http://www.htproducts.com/literature/everlast-electric-tankless-brochure.pdf'),
                new ProductProperty('Use and Care Manual', 'http://www.htproducts.com/literature/198856.pdf'),
                new ProductProperty('Spec and Submittals', 'http://www.htproducts.com/literature/lp-706.pdf'),
                new ProductProperty('Warranty', 'http://www.htproducts.com/literature/198856.pdf#limitedwarranty'),
            ]
        },
    ]
}
const productCategories = Object.freeze([
    {
        name: 'Water Heating',
        id: 'water-heating',
        image: require('../../img/Products/Water-Heating.jpg'),
        products: [],
        navigation: () => Actions.push('ProductCommercialOrResidential', {
            commercialCategory: commercialWaterHeaterCategory,
            residentialCategory: residentialWaterHeaterCategory,
        }),
    },
    {
        name: 'Space Heating',
        id: 'space-heating',
        image: require('../../img/Products/Space-Heating.jpg'),
        products: [],
        navigation: () => Actions.push('ProductCommercialOrResidential', {
            commercialCategory: commercialSpaceHeaterCategory,
            residentialCategory: residentialSpaceHeaterCategory,
        })
    },
    comboApplianceCategory,
    indirectWaterCategory,
    solarCategory, 
    {
        name: 'Comfort Solution',
        id: 'comfort-solution',
        image: require('../../img/Products/ComfortSolutions.jpg'),
        navigation: () => Actions.push('ProductDetail', { 
            product: comfortSolutionProducts[0],
            productCategory: {},
        }),
        products: comfortSolutionProducts,
    }, 
])

//Searching Products. Where Products categories taken from here to the ProductSearch file. ProductSearch.js
const searchableProducts = [
    ..._.flatten(productCategories.map(category => {
        return category.products.map(product => {
            product.categoryName = category.name;
            product.category = category;
            return product;
        });
    })),
    ...commercialSpaceHeaterCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...residentialSpaceHeaterCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...commercialTankGasCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...commercialTankElectricCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...commercialTanklessGasCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...commercialTanklessElectricCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...residentialTanklessGasCategory.products.map(product => {
        const category = residentialTanklessGasCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...residentialTankGasCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...residentialTankElectricCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
    ...residentialTanklessElectricCategory.products.map(product => {
        const category = comboApplianceCategory
        product.categoryName = category.name;
        product.category = category;
        return product;
    }),
];

export { productCategories, commercialSpaceHeaterCategory, residentialSpaceHeaterCategory, searchableProducts };