import {
    StyleSheet,
    Dimensions,
} from 'react-native';

const deviceHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height:deviceHeight/3.5,
        resizeMode:'center',
        width:'100%',
        backgroundColor:'black', 
        padding:2,
        
    },

    Touch:{
        flex:1,
        height: deviceHeight/6, 
        width: '100%' , 
        backgroundColor:'black',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:'1%',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'stretch',
    }
});

export default styles;