import React from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Button, Footer, FooterTab
} from 'native-base';

import { Text } from 'react-native'

const home = {
    text: 'Home',
    navigate: () => Actions.push('Home'),
}

const products = {
    text: 'Products',
    navigate: () =>  Actions.push('Products')
}

function renderBreadcrumb (crumb, key) {
    return (<Button key={key} onPress={() => crumb.navigate()} style={{backgroundColor:'white'}}>
        <Text style={{backgroundColor: 'white', color:'black' , textAlign: 'center'}}>
            {crumb.text}
        </Text>
    </Button>)
}

const _defaultBreadcrumbs = [home, products];

export default function ProductFooter({ breadcrumbs = []}) {
    const getRenderableBreadCrumbs = () => {
        const renderableBreadcrumbs = [..._defaultBreadcrumbs, ...breadcrumbs]
        return renderableBreadcrumbs.length > 3 ? renderableBreadcrumbs.slice(Math.max(renderableBreadcrumbs.length - 3, 1)) : renderableBreadcrumbs
    }
    const renderableBreadCrumbs = getRenderableBreadCrumbs()
    return (
        <Footer>
            <FooterTab>
               {renderableBreadCrumbs.map(renderBreadcrumb)}
            </FooterTab>
        </Footer>
    )
}