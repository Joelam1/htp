import React from 'react'

import {
    View,
    ScrollView,
    Text,
    StatusBar,
    SafeAreaView,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { HomeHeader } from '../../Components/Header';
import {ListItem, Avatar} from 'react-native-elements'
import { searchableProducts } from './products-database';
import styles from './Product.styles';
import { List as ListBase} from 'native-base';

export default class ProductSearchScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        }

        this.searchProducts = this.searchProducts.bind(this);
    }

    goToProduct(product) {
        Actions.push('ProductDetail', { product, productCategory: product.category })
    }

    //Product Images Setting
    renderLeftElement(product){
        return(
            <View>
                <Avatar
                    size = {125}
                    source = {product.image}
                    imageProps={{
                        resizeMode: 'contain',
                        backgroundColor: 'white'
                    }}
                />
            </View>
        )
    }

    //List items of the Products
    renderProduct(product) {
        return (
            <SafeAreaView key={product.id.trim()}>
                    <ListItem
                        onPress={() => this.goToProduct(product)}
                        title={product.name}
                        subtitle={product.model && product.model.length ? product.model.join(', ') : ''}
                        subtitleStyle={{color:'grey',fontSize:10}}
                        leftElement={this.renderLeftElement(product)}
                    />
            </SafeAreaView>
    )   
}

    componentDidUpdate(prevProps) {
        if (this.props.search !== prevProps.search) {
            this.setState({ products: this.searchProducts(this.props.search)})
        }
    }

    searchProducts(search) {
        const searchAbleFields = [
            'name',
            'model',
            'image',
        ];
        const searchingTextWords = search.split(' ').filter(t => t && t !== ' ');
        return searchableProducts.filter(product => {
            return searchAbleFields.some(field => {
                return searchingTextWords.some(text => {
                    const regexp = new RegExp(text, 'ig')
                    return regexp.test(product[field]);
                })
            })
        });
    }

    render(){
        const products = this.state.products;
        return(
            <ScrollView style={{flex:1, backgroundColor:'black'}}>
                <ListBase>
                    {products.map(product => this.renderProduct(product))}
                </ListBase>
            </ScrollView>
            
        );
    }
}
