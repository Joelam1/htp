import {
    StyleSheet,
    Dimensions,
} from 'react-native';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black',
        justifyContent:'space-evenly'
    },

    MenuItem:{
        height:deviceHeight/3.5,
        resizeMode:'center',
        width:'100%',
        backgroundColor:'black', 
        padding:1,
        
    },

    Button:{
        height: deviceHeight/8, 
        width: deviceWidth/2.1, 
        backgroundColor: 'white', 
        alignItems: 'center', 
        justifyContent:'center',
        marginTop: '2%',
        padding: 10,
        borderRadius: 5
    },

    Touch:{
        flex:1,
        height: deviceHeight/7, 
        width: '100%' , 
        backgroundColor:'black',
        alignItems:'center',
        justifyContent:'center',
        marginLeft: '.2%',
        borderRadius:10
    },
    
    Font:{
        fontWeight:'bold', 
        fontSize: 18, 
        textAlign:'center'
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'stretch',
    }
});

export default styles;