import React, { Component } from 'react'

import {
    View,
    StyleSheet,
    Image,
    TouchableHighlight,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { BackHeader } from '../../Components/Header';

//Commercial and Residential Screen. This is only the functionality of the screen. The images are in the product-database.
export default class ProductCommericalOrResidentialScreen extends Component {

    constructor(props) {
        super(props)
    }

    goToProductCategory(productCategory) {
        if (productCategory.navigation) {
            productCategory.navigation()
        } else {
            Actions.push('ProductCategory', { productCategory });
        }
    }

    render() {
        const {
            commercialCategory,
            residentialCategory
        } = this.props
        return (
            <View style={{flex:1, backgroundColor:'black'}}>
                <BackHeader name="Commercial Or Residential" />
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => this.goToProductCategory(residentialCategory)}>
                            <Image
                                    style={styles.Image}
                                    source={residentialCategory.image}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => this.goToProductCategory(commercialCategory)}>
                                <Image
                                    style={styles.Image}
                                    source={commercialCategory.image}
                                />
                            </TouchableHighlight>
                        </View>
                    </View>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height:'25%',
        width: '100%',
        flexDirection:'row',
        backgroundColor:'black', 
        padding:5
    },

    Touch:{
        flex:1,
        height: '100%', 
        width: '100%' , 
        backgroundColor:'black',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'contain',
    }
});