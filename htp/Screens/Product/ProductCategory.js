import React from 'react'

import {
    View,
    ScrollView,
    Image,
    TouchableHighlight,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { BackHeader } from '../../Components/Header';
import styles from './ProductCategory.style';
import ProductFooter from './components/ProductFooter'

export default class ProductCategoryScreen extends React.Component {
    constructor(props) {
        super(props);

        this.renderProductListItem = this.renderProductListItem.bind(this);
        this.getBreadcrumbs = this.getBreadcrumbs.bind(this);
    }

    gotoProductDetail(product) {
        const { productCategory } = this.props
        if (product.navigation) {
            product.navigation()
        } else {
            Actions.push('ProductDetail', { product, productCategory })
        }
    }

    renderProductListItem(product) {
        const { productCategory } = this.props
        return (
            <View style={styles.MenuItem} key={product.id}>
                <TouchableHighlight style={styles.Touch}
                onPress={()=> this.gotoProductDetail(product)}>
                    <Image
                        style={styles.Image}
                        source={product.image}
                    />
                </TouchableHighlight>
            </View>
        )
    }

    getBreadcrumbs() {
        const {
            productCategory: {
                breadcrumbs = []
            }
        } = this.props

        return breadcrumbs.length > 0 ? breadcrumbs.slice(0, breadcrumbs.length - 1) : breadcrumbs
    }

    render(){
        const {
            productCategory: {
                name,
                products,
            },
        } = this.props;
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <BackHeader name={name} />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        {products.map(product => this.renderProductListItem(product))}
                    </View>
                </ScrollView>
                <View style={{marginTop:'.5%'}}>
                    <ProductFooter breadcrumbs={this.getBreadcrumbs()}/>
                </View>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
