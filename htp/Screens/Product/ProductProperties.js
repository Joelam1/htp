import React from 'react'
import {
    View,
    Text,
    ScrollView,
    Image,
    TouchableHighlight,
    StatusBar,
    Linking
} from 'react-native';
import * as _ from 'lodash';
import{ Button }from 'native-base'
import { BackHeader } from '../../Components/Header';
import ProductFooter from './components/ProductFooter'
import styles from './ProductProperties.style'
import { Actions } from 'react-native-router-flux';
export default class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
        
    }

    handleNavigate(productProperty) {
        if (Array.isArray(productProperty.url)) {
            Actions.push('ProductPropertyDetail', {
                productProperty,
                breadcrumbs: this.props.productCategory.breadcrumbs,
                product: this.props.product
            })
        } else {
            Linking.openURL(productProperty.url)
        }
    }

    renderProductPropertyListItem(productProperty) {
        const { styles: styleOverride } = this.props.product
        return (
            <Button style={[styles.Button, styleOverride ? styleOverride.Button : {}]} key={productProperty.property}
            onPress={() => this.handleNavigate(productProperty)}>
                <Text style={[styles.Font, styleOverride ? styleOverride.Font : {}]}>
                    {productProperty.property}
                </Text>
            </Button>
        )
    }
    render(){
        const {
            product,
            productCategory,
        } = this.props;

        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <BackHeader name={product.name} />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}>
                                <Image
                                    style={styles.Image}
                                    source={product.image}
                                />
                            </TouchableHighlight>
                        </View>
                        {product.properties.map(productProperty => this.renderProductPropertyListItem(productProperty))}
                    </View>
                </ScrollView>
                <View style={{marginTop:'.5%'}}>
                    <ProductFooter breadcrumbs={productCategory.breadcrumbs} />
                </View>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
