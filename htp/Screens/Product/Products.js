import React from 'react'

import {
    View,
    ScrollView,
    Image,
    TouchableHighlight,
    StatusBar,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { AppHeader } from '../../Components/Header';

import { productCategories } from './products-database';
import styles from './Product.styles';

export default class ProductsScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            productCategories,
        };

        this.goToCategory = this.goToCategory.bind(this);
        this.renderProductCategoryListItem = this.renderProductCategoryListItem.bind(this);
    }

    goToCategory(productCategory) {
        // If not going to default product category handle that here
        if (productCategory.navigation) {
            productCategory.navigation()
        } else {
            Actions.push('ProductCategory', { productCategory });
        }
    }

    renderProductCategoryListItem(productCategory) {
        return (
            <View style={styles.MenuItem} key={productCategory.id}>
                <TouchableHighlight style={styles.Touch}
                onPress={() => this.goToCategory(productCategory)}>
                    <Image
                      onPress={() => this.goToCategory(productCategory)}
                        style={styles.Image}
                        source={productCategory.image}
                    />
                </TouchableHighlight>
            </View>
        )
    }

    render(){
        const { productCategories } = this.state;
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <AppHeader name="Products" />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        {productCategories.map(productCategory => this.renderProductCategoryListItem(productCategory))}
                    </View>
                <StatusBar barStyle='light-content'/>
                </ScrollView>
            </View>
            
        );
    }
}
