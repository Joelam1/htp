import React, { Component } from 'react';
import { 
    StyleSheet,
    View,
    Text,
    Image,
    StatusBar,
    Linking
} from 'react-native';

import { SocialIcon } from 'react-native-elements'
import { AppHeader } from '../../Components/Header';

class ContactScreen extends Component {
    render(){
        return(
            <View style={{flex:1}}>
                <AppHeader name="Contact Us" />
                <View style={{flex: 1, alignItems: 'center', justifyContent:'center'}}>
                    <Text>{
                        'HTP Comfort Solutions Corporate Office\n'
                        }
                    </Text>

                    <Text>{
                        '272 Duchaine Boulevard\n'
                        }
                    </Text>

                    <Text>
                        {
                        'New Bedford, MA 02745\n'
                        }
                    </Text>

                    <Text>
                        {
                            'Telephone: '
                        }
                        <Text style={{color: 'blue', textDecorationLine:'underline'}}
                        onPress= {() => Linking.openURL('tel://8003239651')}>
                            800 323 9651 {'\n'}
                        </Text>
                    </Text>

                    <Text>
                        {
                            'Telephone: '
                        }
                        <Text style={{color: 'blue', textDecorationLine:'underline'}}
                        onPress= {() => Linking.openURL('tel://5087533769')}>
                            508 763 3769 {'\n'}
                        </Text>
                    </Text>
                    <Text>
                        {
                            'E-Mail: '
                        }
                        <Text style={{color: 'blue', textDecorationLine:'underline'}}
                        onPress= {() => Linking.openURL('mailto:sales@htproducts.com')}>
                            sales@htproducts.com{'\n'}
                        </Text>
                    </Text>

                    <Text>
                        {
                        'Contact Technical Support Page:'
                        }
                    </Text>
                    <Text style={{color: 'blue', textDecorationLine:'underline'}}
                    onPress={() => Linking.openURL('https://www.htproducts.com/technicalsupport.html')}>
                            www.htproducts.com/technicalsupport.html
                    </Text>
                </View>
                <View style={{flex:1, alignItems:'center'}}>
                    <View style={{flex: 1, width:'50%', justifyContent:'center'}}>
                        <SocialIcon 
                            title = 'Twitter'
                            type='twitter'
                            button onPress={() => Linking.openURL('https://twitter.com/HTProductsInc')}
                            /> 
                            <SocialIcon 
                            title = 'Facebook'
                            type='facebook'
                            button onPress={() => Linking.openURL('https://www.facebook.com/HTPinc')}
                            /> 
                            <SocialIcon 
                            title = 'Instagram'
                            type='instagram'
                            button onPress={() => Linking.openURL('https://www.instagram.com/HTPinc')}
                            /> 
                            <SocialIcon 
                            title = 'Youtube'
                            type='youtube'
                            button onPress={() => Linking.openURL('https://www.youtube.com/user/heattransferproducts')}
                            /> 
                            <SocialIcon 
                            title = 'Linkedin'
                            type='linkedin'
                            button onPress={() => Linking.openURL('https://www.linkedin.com/company/heat-transfer-products-inc.')}
                            /> 
                    </View>
                </View>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
export default ContactScreen;