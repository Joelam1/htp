import React from 'react'

import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
    TouchableHighlight,
    StatusBar,
    Dimensions,
    Linking,
} from 'react-native';

import { BackAppHeader } from '../../Components/Header';

var deviceHeight = Dimensions.get("window").height;
export default class Troubleshooting extends React.Component{
    render(){
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <BackAppHeader name="Troubleshooting Videos" />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuXsrYQjemZ4FXAkD7LOucJo')}>
                                <Text>
                                    Crossover Wall
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuVCflkN5a3f3NFGofV9XXYm')}>
                                <Text>
                                    EFT Combi Floor
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuUJ1uPvWHWWT-UqBkFlrsHH')}>
                                <Text>
                                    EFT Combi Wall
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuWJe4JSA21RZlOOLa15Sqii')}>
                                <Text>
                                    Hydra Smart
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuVt1ZPJ0EIlefrhbMkkK1ay')}>
                                <Text>
                                    UFT
                                </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height: deviceHeight/8,
        width:'100%',
        backgroundColor:'black', 
        padding:5
    },

    Touch:{
        flex:1,
        height: deviceHeight/8, 
        width: '100%' , 
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:'1%',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'center',
    }

    });