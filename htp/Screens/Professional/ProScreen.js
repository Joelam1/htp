import React from 'react'

import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
    TouchableHighlight,
    StatusBar,
    Dimensions,
    Linking,
} from 'react-native';

import { AppHeader } from '../../Components/Header';
import { Actions } from 'react-native-router-flux';

var deviceHeight = Dimensions.get("window").height;
export default class ProScreen extends React.Component{
    render(){
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <AppHeader name="Professionals" />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('AllAccess')}>
                                <Text>
                                    {"\t\t"}HTP All Access {"\n"}
                                    For Reps and Wholesalers Only
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Technical')}>
                                <Text>
                                    Technical Bulletins
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={()=>Linking.openURL('http://www.htproducts.com/literature.html')}>
                                <Text>
                                    Discontinued Products
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Training')}>
                                <Text>
                                    Schedule a Training
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Video')}>
                                <Text>
                                    Video Library
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Merchandise')}>
                                <Text>
                                    Merchandise Store
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Photos')}>
                                <Text>
                                    Product Photos
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('SolarLive')}>
                                <Text>
                                    HTP Solar Live
                                </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height: deviceHeight/8,
        width:'100%',
        backgroundColor:'black', 
        padding:5
    },

    Touch:{
        flex:1,
        height: deviceHeight/8, 
        width: '100%' , 
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:'1%',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'center',
    }

    });