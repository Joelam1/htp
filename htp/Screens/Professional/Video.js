import React from 'react'

import {
    View,
    Text,
    ScrollView,
    StyleSheet,
    Image,
    TouchableHighlight,
    StatusBar,
    Dimensions,
    Linking,
} from 'react-native';

import { BackAppHeader } from '../../Components/Header';
import { Actions } from 'react-native-router-flux';

var deviceHeight = Dimensions.get("window").height;
export default class Video extends React.Component{
    render(){
        return(
            <View style={{flex:1, backgroundColor:'black'}}>
                <BackAppHeader name="Video Library" />
                <ScrollView>
                    <View style={styles.MenuContainer}>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuUp54Evij5r4ck6X5fg_4-Z')}>
                                <Text>
                                    Crossover Floor Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuUFhBpwmk-ioE0_AN54c_WG')}>
                                <Text>
                                    Crossover Wall Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuW9VdraZKg1MV0iylysK69G')}>
                                <Text>
                                    Elite Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuXeICODdDDKM4Bf7v5G8-2q')}>
                                <Text>
                                    Elite FT Boiler
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuXiXJn5wSViZDEDqlyz_nQM')}>
                                <Text>
                                    Elite Premier Boiler
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/watch?v=OZLooM2hS20')}>
                                <Text>
                                    Elite Ultra
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/watch?v=6eEhQxn4UO0&list=PLmfVC1MlriuVWSfRwzreAsxMzXJ5ESgh1')}>
                                <Text>
                                    Enduro TI
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuXQIfIl5-5CJ47pK6yV7EQ8')}>
                                <Text>
                                    Phoenix Light Duty Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuWPXNWQrE2DDs14hSwqRsi_')}>
                                <Text>
                                    Phoenix Installation Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuXitGWqHGuo2c0kG_hEs8Vb')}>
                                <Text>
                                    UFT/UFTC
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/watch?v=ukJT3riGtV0&list=PLmfVC1MlriuXDCJ52LGKneEEqPB-r6bCP')}>
                                <Text>
                                    UFT Boiler
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/watch?v=cfuffLjfqeY&list=PLmfVC1MlriuVGMXCh04FBVG3QQlJA3SsX')}>
                                <Text>
                                    Versa Series
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuVrDJhjUDMYTh8xT-1ASCSk')}>
                                <Text>
                                    Water Heater Playlist
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Linking.openURL('https://www.youtube.com/playlist?list=PLmfVC1MlriuUfQC1oq-TxCrjYULgYl2GT')}>
                                <Text>
                                    Faults/Error Codes
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.MenuItem}>
                            <TouchableHighlight style={styles.Touch}
                            onPress={() => Actions.push('Troubleshooting')}>
                                <Text>
                                    Troubleshooting Videos
                                </Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                <StatusBar barStyle='light-content'/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    MenuContainer:{
        flex: 1,
        flexWrap:'wrap', 
        flexDirection:'row',
        backgroundColor:'black'
    },

    MenuItem:{
        height: deviceHeight/8,
        width:'100%',
        backgroundColor:'black', 
        padding:5
    },

    Touch:{
        flex:1,
        height: deviceHeight/8, 
        width: '100%' , 
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        marginLeft:'1%',
        borderRadius:10
    },

    Image:{
        height:'100%',
        width: '100%',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        resizeMode:'center',
    }

    });