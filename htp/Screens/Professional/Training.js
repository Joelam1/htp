import React from 'react';
import { 
	StyleSheet,
	View,
	WebView,
    Image,
	StatusBar,
	Text
} from 'react-native';

import { BackAppHeader } from '../../Components/Header';
export default class Training extends React.Component {

    render() {        
        return (
			<View style={{flex:1}}>
				<BackAppHeader name="Schedule a Training" />
				<View style={styles.layouts}>
					<View style={styles.Screen}>
						<View style={{ width: '100%', height: '95%', marginBottom:'15%', resizeMode:'contain' }}>
							<WebView 
								source={{ uri: "https://htproducts.com/training.html" }}
							/>
						</View>
					</View>
				</View>
                <StatusBar barStyle='light-content'/>
			</View>
        );
    }
}

const styles = StyleSheet.create({

	layouts: {
	    flexDirection: 'row',
	    flexWrap: 'wrap',
	},
	
	Screen: {
	    width: '100%',
		height: '95%',
	},

});