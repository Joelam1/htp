import React from 'react';
import { 
	StyleSheet,
	View,
	WebView,
    Image,
	StatusBar,
	Text
} from 'react-native';

import { BackAppHeader } from '../../Components/Header';
export default class Photos extends React.Component {

    render() {        
        return (
			<View style={{flex:1}}>
				<BackAppHeader name="Product Photos" />
				<View style={styles.layouts}>
					<View style={styles.Screen}>
						<View style={{ width: '100%', height: '95%', marginBottom:'15%', resizeMode:'contain' }}>
							<WebView 
								source={{ uri: "https://www.flickr.com/photos/148563610@N02/sets/72157679485407890/" }}
							/>
						</View>
					</View>
				</View>
                <StatusBar barStyle='light-content'/>
			</View>
        );
    }
}

const styles = StyleSheet.create({

	layouts: {
	    flexDirection: 'row',
	    flexWrap: 'wrap',
	},
	
	Screen: {
	    width: '100%',
		height: '95%',
	},

});