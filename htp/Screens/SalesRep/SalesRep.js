import React from 'react';
import { 
	StyleSheet,
	View,
	WebView,
    Image,
	StatusBar,
	Text
} from 'react-native';

import { AppHeader } from '../../Components/Header';
export default class ShopScreen extends React.Component {

    render() {        
		const setViewPortJS = `var head = document.getElementsByTagName('head')[0];
		head.innerHTML += '<meta name="viewport" content=", width=device-width, initial-scale=.85, minimum-scale = 1.0, maximum-scale = 1.0">';
		document.getElementsByName('Submit')[0].style.backgroundColor = 'black';
		document.getElementsByName('Submit')[0].style.color = 'white';;
		`;
        return (
			<View style={{flex:1}}>
				<AppHeader name="Sales Rep" />
				<Text style={{textAlign:'center', fontSize: 28, alignItems:'center', justifyContent:'center', marginTop: '5%' }}>
						Find a HTP {'\n'}
					Sales Representative {'\n'}
				</Text>
				<View style={styles.layouts}>
					<View style={styles.Screen}>
						<View style={{ width: '100%', height: '85%', marginBottom:'25%', resizeMode:'contain' }}>
							<WebView 
								injectedJavaScript={setViewPortJS}
								source={{ uri: "http://www.htproducts.com/find-sales-representative.php" }}
							/>
						</View>
					</View>
				</View>
                <StatusBar barStyle='light-content'/>
			</View>
        );
    }
}

const styles = StyleSheet.create({

	layouts: {
	    flexDirection: 'row',
	    flexWrap: 'wrap',
	},
	
	Screen: {
	    width: '100%',
		height: '95%',
	},

});