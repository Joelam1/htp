import React from 'react';

import { Router, Scene, Drawer, Actions, Stack } from 'react-native-router-flux';

//import New Screen Here
import HomeScreen from '../Screens/Home/HomeScreen'
import ContactScreen from '../Screens/Contact/ContactScreen'
import ShopScreen from '../Screens/Shop/ShopScreen'
import SalesRep from '../Screens/SalesRep/SalesRep'
import WarrantyScreen from '../Screens/Warranty/WarrantyScreen'
import ProScreen from '../Screens/Professional/ProScreen'
import AllAccess from '../Screens/Professional/AllAccess'
import Merchandise from '../Screens/Professional/Merchandise'
import Photos from '../Screens/Professional/Photos'
import SolarLive from '../Screens/Professional/SolarLive'
import Technical from '../Screens/Professional/Technical'
import Training from '../Screens/Professional/Training'
import Video from '../Screens/Professional/Video'
import Troubleshooting from '../Screens/Professional/Troubleshooting'
import Extend from '../Screens/Warranty/Extend'
import WarReg from '../Screens/Warranty/WarReg'
import Wizard from '../Screens/Warranty/Wizard'

import ProductCategoryScreen from '../Screens/Product/ProductCategory';
import ProductsScreen from '../Screens/Product/Products';
import ProductPropertyScreen from '../Screens/Product/ProductProperties';
import ProductCommericalOrResidentialScreen from '../Screens/Product/ProductCommericalOrResidential';

import { Container, Content, List, ListItem, Text } from 'native-base';
import { Image, View, YellowBox } from 'react-native';
import ProductSearchScreen from '../Screens/Product/ProductSearch';

//This ignore warning is created because of dependencies that has been downloaded to this program, that causes a small warning on android.
//But do not worry because 'ViewPagerAndroid' are not been used on any part of this applications, this is just to prevent the warning from showing. 
YellowBox.ignoreWarnings(['ViewPagerAndroid']);

//This will be the list of items that are on the Menu Drawer of the Application
const MainDrawer = function (props) {
    return (
        <Container>
            <List>
                <ListItem>
                        <Image style ={{
                            flex: 1,
                            width: 75,
                            height: 75,
                            marginTop:'15%',
                            borderRadius: 20,
                            overflow:'hidden',
                            backgroundColor:'black',
                            resizeMode: 'contain' }}
                            source={require('../img/Logo.png')}
                        />
                </ListItem>

                <ListItem onPress={() => {Actions.Home()}}>
                    <Text>Home</Text>
                </ListItem>

                <ListItem onPress={() => Actions.Products()}>
                    <Text>Products</Text>
                </ListItem>

                <ListItem onPress={() => Actions.Shop()}>
                    <Text>Shop Parts</Text>
                </ListItem>

                <ListItem onPress={() => Actions.Warranty()}>
                    <Text>Warranty</Text>
                </ListItem>
                
                <ListItem onPress={() => Actions.Professional()}>
                    <Text>Professional</Text>
                </ListItem>

                <ListItem onPress={() => Actions.Contact()}>
                    <Text>Contact Us</Text>
                </ListItem>
            </List>
        </Container>
    );
}

//This is where the roots and stacks of screens that navigates in between screens
export default function () {
    return (
    <Router>
        <Scene key="root" hideNavBar={true}>
            <Drawer contentComponent={MainDrawer} key="mainDrawer">
                <Scene
                    hideNavBar={true}
                    component={HomeScreen}
                    key="Home"
                    initial={true}
                />
                <Scene
                    hideNavBar={true}
                    key="Sales"
                    component={SalesRep}
                />
                <Scene
                    hideNavBar={true}
                    key="Shop"
                    component={ShopScreen}
                />
                <Scene
                    hideNavBar={true}
                    key="Professional"
                    component={ProScreen}
                />
                <Scene
                    hideNavBar={true}
                    key="Contact"
                    component={ContactScreen}
                />
                <Stack>
                    <Scene
                        hideNavBar={true}
                        key="Home"
                        component={HomeScreen}
                    />
                    <Scene
                        hideNavBar={true} 
                        key="Products"
                        component={ProductsScreen}
                    />

                    <Scene 
                        hideNavBar={true}
                        key="ProductCategory"
                        component={ProductCategoryScreen}
                    />

                    <Scene 
                        hideNavBar={true}
                        key="ProductDetail"
                        component={ProductPropertyScreen}
                    />

                    <Scene
                        hideNavBar={true} 
                        key="ProductCommercialOrResidential"
                        component={ProductCommericalOrResidentialScreen}
                    />
                    <Scene
                        hideNavBar={true}
                        key="AllAccess"
                        component={AllAccess}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Merchandise"
                        component={Merchandise}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Photos"
                        component={Photos}
                    />
                    <Scene
                        hideNavBar={true}
                        key="SolarLive"
                        component={SolarLive}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Technical"
                        component={Technical}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Training"
                        component={Training}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Video"
                        component={Video}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Troubleshooting"
                        component={Troubleshooting}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Warranty"
                        component={WarrantyScreen}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Extend"
                        component={Extend}
                    />
                    <Scene
                        hideNavBar={true}
                        key="WarReg"
                        component={WarReg}
                    />
                    <Scene
                        hideNavBar={true}
                        key="Wizard"
                        component={Wizard}
                    />
                </Stack>
            </Drawer>
        </Scene>
    </Router>
    );
}

