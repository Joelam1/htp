import React from 'react';
import { 
    StyleSheet,
    View,
    StatusBar,
    Text,
} from 'react-native';
import { AppLoading } from "expo";
import * as Font from 'expo-font';

import DrawNav from './Navigation/DrawNav'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }


  render(){
    return this.state.loading ? <AppLoading></AppLoading> : (
      <View style={styles.container}>
        <DrawNav/>
        <StatusBar barStyle='light-content'/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});